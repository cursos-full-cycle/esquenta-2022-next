import type { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios';

type Data = {
    name: string
}

const handler = async (
    req: NextApiRequest,
    res: NextApiResponse<Data>
) => {
    await axios.post('http://localhost:8000/players', { name: req.body.name });
    await res.revalidate('/players/isr_on_demand');
    // opcional: cacheia a nova resposta por um período em segundos
    res.setHeader('stale-while-revalidate', '60')
    return res.status(204);
}

export default handler