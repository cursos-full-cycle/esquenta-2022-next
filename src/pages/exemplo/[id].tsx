import { Router, useRouter } from "next/router"

const Exemplo1 = () => {
    const route = useRouter()
    return (
        <h1>Página de exemplo - {route.query.id}</h1>
    )
}

export default Exemplo1