import { useEffect, useState } from "react"
import axios from 'axios'
import { Player } from "../../utils/models"

const PlayersListStaticPage = () => {
    const [players, setPlayers] = useState<Player[]>([] as Player[])

    useEffect(() => {
        axios
            .get('http://localhost:8000/players')
            .then(result => setPlayers(result.data))
    }, [])

    return (
        <>
            <h1>Jogadores Disponíveis</h1>
            <ul>
                {players.map((player) => <li key={player.id}>{player.name}</li>)}
            </ul>
        </>
    )
}

export default PlayersListStaticPage