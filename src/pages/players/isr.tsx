import { GetStaticProps } from "next"
import { Player } from "../../utils/models"
import axios from 'axios'

type PlayersListISRPageProps = {
    players: Player[]
}

const PlayersListISRPage = ({players}: PlayersListISRPageProps) => {


    return (
        <>
            <h1>Jogadores Disponíveis</h1>
            <ul>
                {players.map((player) => <li key={player.id}>{player.name}</li>)}
            </ul>
        </>
    )
}

// roda props estáticos no BUILD, fornecendo a mesma página pelo tempo definindo (revalidate), atualizando a cada passagem desse tempo
export const getStaticProps: GetStaticProps = async (ctx) => {
    const { data } = await axios.get('http://localhost:8000/players')
    return {
        props: {
            players: data
        },
        revalidate: 40,
    }
}

export default PlayersListISRPage