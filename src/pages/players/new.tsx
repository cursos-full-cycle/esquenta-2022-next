import axios from "axios"
import { useState } from 'react';

const NewPlayerPage = () => {
    const [name,setName] = useState('')
    const [loading, setLoading] = useState(false)

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        setLoading(true)
        if (!!name.trim()){
            try {
                await axios.post('http://localhost:3000/api/new_player', { name })
                alert('Jogador cadastrado com sucesso!')
            } catch (error) {
                alert(`Erro:\n${error}`)
            }
        } else {
            alert('Por favor, digite um nome válido.')
        }
        setName('')
        setLoading(false)
    }

    return (<>
        <h1>Cadastrar novo Jogador</h1>
        <form onSubmit={handleSubmit}>
            <input type="text" name="name" id="playerName" placeholder='nome do jogador' value={name} onChange={({target})=>{setName(target.value)}}/>
            {
                !!loading
                ? <p>Loading...</p>
                : <p><input type="submit" value="Cadastrar" /></p>
            }
        </form>
    </>)
}
export default NewPlayerPage