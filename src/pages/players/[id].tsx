import { useEffect, useState } from "react"
import axios from 'axios'
import { useRouter } from "next/router";
import { Player } from "../../utils/models";

const PlayersListPage = () => {
    const [player, setPlayer] = useState<Player>({} as Player)
    const route = useRouter()
    let { ID } = route.query
    // @ts-ignore
    const id = parseInt(ID)

    useEffect(() => {
        const notFound: Player = {
            id,
            name: "Jogador Não encontrado"
        }
        try {
            axios
                .get('http://localhost:8000/players')
                .then(result => {
                    return result.data.filter((j: Player) => j.id == id)[0]
                })
                .then(res => setPlayer(res || notFound))
        } catch (error) {
            return (
                alert(`Erro:\n${error}`)
            )
        }
    }, [id])

    return (
        <>
            <h1>Página do Jogador - número {id}</h1>
            {!!player
                ? <h2>{player.name}</h2>
                : <h3>Loading...</h3>
            }
        </>
    )
}

export default PlayersListPage