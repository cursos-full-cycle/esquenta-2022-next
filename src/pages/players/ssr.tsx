import axios from 'axios'
import { Player } from "../../utils/models"
import { GetServerSideProps } from "next"

type PlayersListSSRPageProps = {
    players: Player[]
}

const PlayersListSSRPage = ({ players }: PlayersListSSRPageProps) => {

    return (
        <>
            <h1>Jogadores Disponíveis</h1>
            <ul>
                {players.map((player) => <li key={player.id}>{player.name}</li>)}
            </ul>
        </>
    )
}

// Atualiza no RUNTIME, ou seja, a cada requisição, atualiza (no servidor)
// O nome dessa função DEVE ser getServerSideProps
export const getServerSideProps: GetServerSideProps = async (ctx) => {
    const { data } = await axios.get('http://localhost:8000/players')
    return {
        props: {
            players: data
        }
    }

}

export default PlayersListSSRPage