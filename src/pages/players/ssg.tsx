import { GetStaticProps } from "next"
import { Player } from "../../utils/models"
import axios from 'axios'

type PlayersListSSGPageProps = {
    players: Player[]
}

const PlayersListSSGPage = ({players}: PlayersListSSGPageProps) => {


    return (
        <>
            <h1>Jogadores Disponíveis</h1>
            <ul>
                {players.map((player) => <li key={player.id}>{player.name}</li>)}
            </ul>
        </>
    )
}

// roda props estáticos no BUILD, não altera o conteúdo, fornecendo sempre a mesma página
export const getStaticProps: GetStaticProps = async (ctx) => {
    const { data } = await axios.get('http://localhost:8000/players')
    return {
        props: {
            players: data
        }
    }
}

export default PlayersListSSGPage